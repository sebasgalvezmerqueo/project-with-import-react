import "./App.css";
import { LibraryReactExport2 } from "@merqueo/library-react-export-2";

function App() {
  return (
    <div className="App">
      <h1>From React Project</h1>
      <LibraryReactExport2 />
    </div>
  );
}

export default App;
